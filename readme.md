##Synopsis

This is a Generic .gitignore file for Wordpress Based Projects. It ignores the **BARE MINIMUM** to have a functional installation working, so you may want to extend it.

## What does this file ignore?

This file allows you to ignore your IDE files, wp-config, .htacces, sass-cache files, default Wordpress themes and is prepared to ignore cache files.

## Motivation

A few days ago, at work I found a Wordpress repo that was quite hard to work with, as the gitignore wasn't properly set. So I tried to find a gitignore file that allowed me to work with no worries and i didn't find it. That's the reason why I made this gitignore file and now I share it with you.

## You are free to contribute

You are not just free, you are encouraged to make this file the best gitignore for Wordpress Environments **EVER**.

## Contributors

At this moment it's just me, you can find me on twitter by @jarredethe.

## License

The MIT License (MIT)